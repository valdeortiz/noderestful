const { Pool } = require('pg')

const pool = new Pool({
	host: 'localhost',
	user: 'postgres',
	password: 'password',
	database: 'firstapi',
	port: '5432'
});

const getUsers = async (req, res) => {
	const response = await pool.query('SELECT * FROM users ORDER BY id ASC');
	res.status(200).json(response.rows);
};

const getUserById = async ( req, res ) => {
	const id = parseInt(req.params.id);
	const response = await pool.query('SELECT * FROM users WHERE id = $1', [id]);
	res.json(response.rows);
};

const createUsers = async (req, res) => {
	const { name, email } = req.body;
	const response = await pool.query('INSERT INTO users (name,email) VALUES ($1,$2)', [name,email]);
	res.json({
		message: 'user created',
		body: {
			user: {name,email}
		}
	})
};

const updateUser = async (req, res) => {
	const id = parseInt(req.params.id);
	const { name, email } = req.body;
	const response = await pool.query('UPDATE users SET name = $1, email = $2 WHERE id = $3', [name,email,id]);
	res.json('user updated successfuly');
};

const deleteUser = async (req, res) => {
	const id = parseInt(req.params.id);
	const response = await pool.query('DELETE FROM users WHERE ID = $1', [id]);
	res.json(`user ${id} deleted successfuly`);
};

module.exports = {
	getUsers,
	createUsers,
	getUserById,
	deleteUser,
	updateUser
}