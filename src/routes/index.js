const { Router } = require('express');
const router = Router();

const { getUsers, createUsers, getUserById, updateUser, deleteUser } = require('../controllers/indexController');

router.get('/users', getUsers);
router.post('/users', createUsers);
router.get('/users/:id', getUserById);
router.delete('/users/:id', deleteUser);
router.put('/users/:id', updateUser);

module.exports = router;
