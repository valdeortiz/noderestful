# Restful api con Nodejs. 🚀

[![version](https://img.shields.io/badge/Version-v1.0-blue)](https://github.com/valdeortiz/)

## Descarga de Repositorio :arrow_backward:

Ejecutar:``` git clone https://github.com/valdeortiz/noderestful ```

o decargar en el repositorio. En la seccion de clone, click en ***Descargar en zip***


***
## Pre-requisitos 📋.
Se necesita tener instalado Nodejs junto con express y pg. 
Ejecutar: 
> ```npm i express pg```

- [Nodejs Download](https://nodejs.org/en/download/)
- Un editor de texto(atom, vscode, etc).
- [Insomnia Rest client](https://insomnia.rest/)

Seguir las intruciones del enunciado anterior para la Descarga del repositorio.


### Ejecucion 🔩
Ejecutamos el siguiente comando en el directorio para levantar nuestro servidor.

> ```cd noderestful/src```

> ```node index.js```

***


## Documentacion oficial 📄
[Node-Documentacion](https://nodejs.org/en/docs/ "Documentacion oficial de Nodejs")

***

## EndPoints ⌨️

1. Motrar todos los usuarios(findAll).

<details>

**Descripcion:**
Muestra en formato tipo json todos los usuarios. 

**Argumentos:**
- Ninguno.

Ejemplos:

``` http://localhost:3000/users/ ```

**Metodo HTTP:**
GET

</details>

2. Mostrar un usuario especifico(findById).

<details>

**Descripcion:**
Muestra los datos de un usuario especifico de la entidad.

**Argumentos:**
- id: id unico del usuario.OBS: en caso de no saber el id, ejecutar el findAll.

Ejemplos:

``` http://localhost:3000/users/{id} ```

**Metodo HTTP:**
GET

</details>

4. Agregar un usuario.

<details>

**Descripcion:**
Agrega un usuario de la base de datos de la entidad.

**Argumentos:**

```
{
	"name":"",
	"email":""
}
```


Ejemplos:

``` http://localhost:3000/users/ ```

**Metodo HTTP:**
POST

</details>

4. Borrar un usuario segun su id(deleteById).

<details>

**Descripcion:**
Borra un usuario de la base de datos de la entidad.

**Argumentos:**

- id: id unico del usuario a ser eliminado.

Ejemplos:

``` http://localhost:3000/users/id ```

**Metodo HTTP:**
DELETE

</details>

5. Actualizar un usuario segun su id(deleteById).

<details>

**Descripcion:**
Actualiza un usuario de la base de datos de la entidad.

**Argumentos:**

```
{
	"name":"",
	"email":""
}
```


Ejemplos:

``` http://localhost:3000/users/id ```

**Metodo HTTP:**
PUT

</details>


***

## Recomendaciones 📦

- Asegurarse de tener todo descargado correctamente.
- Tener conocimientos basicos en el protocolo http.
- Asegurarse de tener todas las dependencias instaladas en su maquina, recomendamos la lectura de pre-requisitos.

## Autores ✒️

* **Valdemar Ortiz** - [valdeortiz](https://github.com/valdeortiz)
